var
	gulp = require('gulp'),
	//development mode
	devBuild = (process.env.NODE_ENV !== 'production')
	// folders
	//folder = {
	//	src: 'src/',
	//        build: 'build/'
	//}
	;
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer=require('gulp-autoprefixer');
var sassdoc = require('sassdoc');

//var input = './src/scss/**/*.scss';
var input = './src/scss/*.scss';
var output = './public/css';

gulp.task('sass', function () {
	return gulp
	// Find all `.scss` files from the `stylesheets/` folder
	.src(input)
	// Run Sass on those files
	.pipe(sourcemaps.init())	
	.pipe(sass(sassOptions).on('error', sass.logError))
	// Write the resulting CSS in the output folder
	.pipe(sourcemaps.write())
	.pipe(autoprefixer())
	.pipe(gulp.dest(output));
	//.pipe(sassdoc())
	// Release the pressure back and trigger flowing mode (drain)
	// See: http://sassdoc.com/gulp/#drain-event
	//.resume();
	});

gulp.task('sassdoc', function () {
	  return gulp
	    .src(input)
	    .pipe(sassdoc())
	    .resume();
});

gulp.task('watch', function() {
	  return gulp
	    // Watch the input folder for change,
	    // and run `sass` task when something happens
	    .watch(input, ['sass'])
	    // When there is a change,
	    // log a message in the console
	    .on('change', function(event) {
	    console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
	});
});

gulp.task('default', ['sass', 'watch' /*, possible other tasks... */]);

var sassOptions = {
	  errLogToConsole: true,
	  outputStyle: 'expanded'
};
